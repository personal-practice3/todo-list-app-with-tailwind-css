module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      transitionProperty: {
        top : "top"
      }
    },
  },
  plugins: [],
}
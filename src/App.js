import {useState} from 'react'
import './App.css';
import Form from './Components/Form';
import Navbar from './Components/Navbar';
import Todo from './Components/Todo';

function App() {

const [input, setInput] = useState('')
const [todos, setTodos] = useState([])

  return (
      <>
      <Navbar/>

      <div className="sm:container lg:w-3/5 md:mx-auto lg:container lg:mx-auto"  >
      <Form 
      input ={input} 
      todos = {todos}
      setInput = {setInput} 
      setTodos= {setTodos} 
    
    
      />

      <Todo 
      todos = {todos}
      setTodos= {setTodos} 
      />
      </div>
      </>
  );
}

export default App;

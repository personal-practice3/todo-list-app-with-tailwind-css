/* eslint-disable array-callback-return */
import React, { useState } from 'react'

export default function TodoList({Todos,id, todos, setTodos,}) {
  
const [edit, setEdit] = useState(false)
const [updateEdit, setUpdateEdit] = useState(Todos.text)
  
  const onDeleteHandler = () => {
     let result = todos.indexOf(Todos)
     const newTodos = [...todos]
    newTodos.splice(result,1)
    setTodos(newTodos)
    }
  
  const onEditHandler = () => {
      setEdit(true)
  }
  const onCancelHandler = () => {
        setEdit(false)
  }

  const onUpdateChangeHandler = (e) => {
        setUpdateEdit(e.target.value)
  }

  const onSaveEditHandler = (id) => {
        todos.find(result => {
            if(result.id === id){
                if(updateEdit !== ''){
                    result.text = updateEdit
                }
            }
        })
        setEdit(false)
  }
  
    return (
    <div>
        {
            edit ?

            
            <div className='w-full flex justify-between'>
                    <div>
                        <input 
                        value={updateEdit}
                        onChange={onUpdateChangeHandler}
                        className="border border-black-800 my-2"
                        autoFocus
                        />
                    </div>
            
                    <div>
                        <button 
                        className='bg-green-600 hover:bg-green-700 text-white p-2 mx-2 my-1 border rounded'
                        onClick={e => onSaveEditHandler (Todos.id)}
                        >Save</button>
                        
                        <button 
                        className='bg-gray-500 hover:bg-gray-600 text-white  p-2 mr-4 border rounded'
                        onClick={onCancelHandler}>Cancel</button>
                    </div>
            </div>

            :

            <div className='flex justify-between w-full'>
            <div>
            <p className='my-1 p-1'>{Todos.text}</p>
            </div>
            <div className='px-4'>
            <button className='bg-sky-600 hover:bg-sky-700 text-white border rounded  p-2 mx-2 my-1' onClick={onEditHandler}>Edit</button>
            <button className='bg-red-600 hover:bg-red-700 text-white border rounded  p-2 ' onClick={onDeleteHandler}>Delete</button>
            </div>
            </div>
        }

        
    </div>
  )
}

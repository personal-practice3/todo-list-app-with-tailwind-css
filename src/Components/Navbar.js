import React, {useState} from 'react'
import {HiOutlineMenu} from 'react-icons/hi'


export default function Navbar() {

    const[isOpen, setIsOpen] = useState(false)

  return (
      <nav className='md:flex justify-between items-center bg-gray-800 sticky top-0 z-20 py-2'>
         <div className='flex items-center justify-between'>
         <a href="/" className='font-bold text-lg px-3 text-orange-400'>IT<span className='text-white'>Solutions</span></a>
            <HiOutlineMenu onClick={()=> setIsOpen(!isOpen)} className='md:hidden block w-10 h-10 p-2 cursor-pointer text-white'/>
          </div>

     
            <ul className={(isOpen ? "top-12" : "-top-full") +
            " md:static fixed top-12 md:flex md:space-x-7 items-center md:bg-transparent bg-gray-800 bg-opacity-90 md:w-auto w-screen md:text-white text-white md:space-y-0 space-y-5 text-center p4 px-5 transition-top"}>
            <li className='text-lg font-semibold hover:text-slate-400'><a href='#'>Home</a></li>
            <li className='text-lg font-semibold hover:text-slate-400'><a href='#'>About</a></li>
            <li className='text-lg font-semibold hover:text-slate-400'><a href='#'>Skills</a></li>
            <li className='text-lg font-semibold hover:text-slate-400'><a href='#'>Contact</a></li>
            </ul>

         
      </nav>

  )
}

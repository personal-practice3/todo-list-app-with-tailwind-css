import React, { useEffect } from 'react'
import TodoList from './TodoList'

export default function Todo({todos, setTodos,}) {


  return (
    <div className=' lg:w-content border-2 border-dashed border-black-800 p-4 md:w-full'>
        {

          (todos.length===0) ? 
          <>
          <p>no items yet, please add your first Item</p>
          </>
          :
            todos.map(Todos => 
                <TodoList
                key = {Todos.id}
                id = {Todos.id}
                todos = {todos}
                setTodos= {setTodos} 
                Todos = {Todos}
                />
                )
        }
    </div>
  )
}

import React, { useEffect } from 'react'

export default function Form({setInput,input,todos, setTodos}) {


    const checkSpaces = (inputText) => {
        return /^\s*$/.test(inputText);
      }
    const onChangeHandler = (e) =>{
        setInput(e.target.value)
    }
    const onSubmitHandler = (e) => {
        e.preventDefault ();
       
        if(input === ''){
            let color=document.querySelector("#input-form-text")
            color.style.borderColor = "red"
        }
        else {
            let result = checkSpaces(input)

            if(result){
            let color=document.querySelector("#input-form-text")
            color.style.borderColor = "red"
            }
          
            else{
                let text=input.toLowerCase()
                if(text==="clear"){
                    setInput('')
                }
                else{
                let color=document.querySelector("#input-form-text")
                color.style.borderColor = 'gray'

                setTodos([...todos, 
                    { 
                        text : input, 
                        id: new Date(),
                        
                    }])
                setInput('')
                }
                
            }
        }
    }

    const clearAllList = () => {
        
        let text = input.toLowerCase()
        let i = todos.length
        if(text === "clear")
            todos.splice(0,i)
        }
        
    useEffect (() => {
        clearAllList()
    },[onSubmitHandler,]) 

  return (
    <div className='form-container flex justify-center mx-auto'>
        <form onSubmit={onSubmitHandler}>
            <input
            type="text"
            name="text"
            placeholder="Enter Todo"
            onChange={onChangeHandler}
            value={input}
            id="input-form-text"
            className='border border-black-800 focus:border-red-600'
            />
            <button 
            type='submit'
            className = "bg-orange-400 p-1 my-1 hover:bg-orange-500 border rounded" 

            >ADD</button>
        </form>
    </div>
  )
}
